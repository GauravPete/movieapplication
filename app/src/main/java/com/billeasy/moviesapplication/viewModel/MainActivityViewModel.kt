package com.billeasy.moviesapplication.viewModel

import androidx.lifecycle.*
import androidx.paging.cachedIn
import com.billeasy.moviesapplication.networkModel.MovieItem
import com.billeasy.moviesapplication.repository.MoviesRepository

class MainActivityViewModel(
    private val repository: MoviesRepository
): ViewModel() {
   private val currentMovies =MutableLiveData<List<MovieItem>>()


    val movies =currentMovies.switchMap {
        repository.getSearchResult().cachedIn(viewModelScope)
    }
}