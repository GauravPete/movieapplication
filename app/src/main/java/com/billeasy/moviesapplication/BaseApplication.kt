package com.billeasy.moviesapplication

import android.app.Application
import com.billeasy.moviesapplication.di.retrofitModule
import org.koin.core.context.startKoin

class BaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(retrofitModule)
        }
    }

}