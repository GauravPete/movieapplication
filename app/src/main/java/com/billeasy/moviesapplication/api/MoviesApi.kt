package com.billeasy.moviesapplication.api

import com.billeasy.moviesapplication.networkModel.MoviesDetailsResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesApi {
    @GET("/3/movie/now_playing")
    suspend fun getMoviesList(
        @Query("api_key") apiKey :String,
        @Query("language") language : String,
        @Query("page") page : Int
    ):MoviesDetailsResponse
}