package com.billeasy.moviesapplication.di

import com.billeasy.moviesapplication.api.MoviesApi
import com.billeasy.moviesapplication.networkModel.MovieItem
import com.billeasy.moviesapplication.repository.MoviesRepository
import com.billeasy.moviesapplication.utility.ApiConstans
import com.billeasy.moviesapplication.viewModel.MainActivityViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun retrofitProvider(): Retrofit =
    Retrofit.Builder()
        .baseUrl(ApiConstans.BASEURL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

fun moviesApiServiceProvider(retrofit: Retrofit): MoviesApi =
    retrofit.create(MoviesApi::class.java)

val retrofitModule = module {
    single { retrofitProvider() }
    factory { moviesApiServiceProvider(get()) }
    single { MoviesRepository(get()) }
    viewModel { MainActivityViewModel(get()) }

}


