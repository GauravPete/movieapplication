package com.billeasy.moviesapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.billeasy.moviesapplication.databinding.MovieItemlistLayoutBinding
import com.billeasy.moviesapplication.networkModel.MovieItem
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class MovieDataAdapter :PagingDataAdapter<MovieItem,MovieDataAdapter.MovieDataHolder>(MOVIES_COMPARETOR) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieDataHolder {
       val binding = MovieItemlistLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MovieDataHolder(binding)
    }
    override fun onBindViewHolder(holder: MovieDataHolder, position: Int) {
       val movieItem = getItem(position)
        if(movieItem!=null){
            holder.bind(movieItem)
        }
    }


    class MovieDataHolder(private val binding:MovieItemlistLayoutBinding) : RecyclerView.ViewHolder(binding.root){


        fun bind(movie : MovieItem){
            binding.apply {
                Glide.with(itemView)
                    .load("https://image.tmdb.org/t/p/original${movie.poster_path}")
                    .centerCrop()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(movieImage)

                textView.text=movie.title

            }
        }
    }
    companion object{
        private val MOVIES_COMPARETOR = object : DiffUtil.ItemCallback<MovieItem>(){
            override fun areItemsTheSame(oldItem: MovieItem, newItem: MovieItem)=
                oldItem.id==newItem.id

            override fun areContentsTheSame(oldItem: MovieItem, newItem: MovieItem)=
                oldItem==newItem
        }
    }


}