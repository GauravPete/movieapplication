package com.billeasy.moviesapplication.repository


import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.billeasy.moviesapplication.api.MoviesApi
import com.billeasy.moviesapplication.pagingSource.MoviesPagingSource


class MoviesRepository (private val moviesApi: MoviesApi) {

    fun getSearchResult()=
        Pager(
            config = PagingConfig(
                pageSize = 20,
                maxSize = 100,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {MoviesPagingSource(moviesApi)}
        ).liveData
}