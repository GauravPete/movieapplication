package com.billeasy.moviesapplication.networkModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieItem(
    val id: Int,
    val poster_path: String,
    val title: String,
    val vote_average: String
) : Parcelable {

}
