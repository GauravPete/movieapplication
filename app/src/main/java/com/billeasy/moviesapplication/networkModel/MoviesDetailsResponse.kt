package com.billeasy.moviesapplication.networkModel

data class MoviesDetailsResponse(
    val results : List<MovieItem>
)
