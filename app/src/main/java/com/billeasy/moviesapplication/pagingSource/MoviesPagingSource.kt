package com.billeasy.moviesapplication.pagingSource

import androidx.paging.PagingSource
import com.billeasy.moviesapplication.api.MoviesApi
import com.billeasy.moviesapplication.networkModel.MovieItem
import com.billeasy.moviesapplication.utility.ApiConstans
import retrofit2.HttpException
import java.io.IOException

private const val MOVIES_STARTING_PAGE_INDEX=1

class MoviesPagingSource(
    private val moviesApi: MoviesApi,
   /* private val query : String*/
) : PagingSource<Int,MovieItem>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MovieItem> {
        val position =params.key ?: MOVIES_STARTING_PAGE_INDEX
        return try{
            val response = moviesApi.getMoviesList(ApiConstans.apiKey,ApiConstans.LANG,position)
            val movies = response.results

            LoadResult.Page(
                data = movies,
                prevKey = if(position== MOVIES_STARTING_PAGE_INDEX) null else position-1,
                nextKey = if(movies.isEmpty()) null else position+1,
            )
        }catch (ex : IOException){
            LoadResult.Error(ex)
        }catch (ex : HttpException){
            LoadResult.Error(ex)
        }

    }
}