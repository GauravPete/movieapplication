package com.billeasy.moviesapplication.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.billeasy.moviesapplication.R
import com.billeasy.moviesapplication.adapter.MovieDataAdapter
import com.billeasy.moviesapplication.databinding.ActivityMainBinding
import com.billeasy.moviesapplication.viewModel.MainActivityViewModel
import kotlinx.android.synthetic.main.movie_itemlist_layout.*
import org.koin.android.ext.android.get
import org.koin.core.context.GlobalContext.get

class MainActivity : AppCompatActivity() {
    var viewModel= get<MainActivityViewModel>()
    private var _binding : ActivityMainBinding?=null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        _binding=ActivityMainBinding.bind(view)

        val adapter= MovieDataAdapter()
        binding.apply {
            moviesListItem.setHasFixedSize(true)
            moviesListItem.adapter=adapter
        }

        viewModel.movies.observe(this, Observer {
          // adapter.submitData(it)
        })




    }
}